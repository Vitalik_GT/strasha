<?php

class PacketManager
{
	public function LoadPackets ()
	{
		$packets = glob ('Packets\*.epe');

		foreach ($packets as $id => $packet)
			$return[$packet] = self::LoadPacket ($packet);

		return $return;
	}

	public function LoadPacket ($file)
	{
		if ($info = self::getPacketInfo ($file))
		{
			$memory = fopen ('php://memory', 'w+');
			fwrite ($memory, $info['content']);
			fseek ($memory, 0);
			bcompiler_read ($memory);
			fclose ($memory);

			return true;
		}

		else return false;
	}

	public function getPacketInfo ($packet)
	{
		$eap = new EAP ($packet);

		$info = $eap->getEntitiesList ('/workspace');

		if (is_array ($info))
		{
			foreach ($info as $id => $value)
				$info[$id] = $eap->unpackText ($value);

			return $info;
		}

		else return false;
	}
}

?>
