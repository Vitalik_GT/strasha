<?php

/*
    Enfesto Archive Packet (EAP) Engine
    Copyright © 2018 Podvirnyy Nikita (KRypt0n_)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

    and

    Enfesto Studio Group license
    https://vk.com/topic-113350174_36400959

    -----------------------

    Contacts:

    Email: <suimin.tu.mu.ga.mi@gmail.com>
    VK:    vk.com/technomindlp
           vk.com/hphp_convertation
*/

class EAP
{
	public $packet; // Массив пакета

	/*
		string [@file = false] - путь к файлу или содержимое самого файла EAP-пакета

		Если значение будет равно false, то пакет инициализируется с нуля
	*/

	public function __construct ($file = false) // Открытие пакета
	{
		if (file_exists ($file))
			self::fileOpen ($file);

		elseif ($file)
			self::readData ($file);

		else $this->packet = array ();
	}

	/*
		string @file - путь к файлу пакета
	*/

	public function fileOpen ($file) // Открывает пакет через файл
	{
		$this->packet = self::unpackText (file_get_contents ($file), false);
	}

	/*
		string @data - содержимое пакета
	*/

	public function readData ($data) // Открывает пакет через содержимое
	{
		$this->packet = self::unpackText ($data, false);
	}

	/*
		string @file             - путь для сохранения
		mixed [@data = array ()] - данные (пустой массив будет воспринят как создание пустой папки)
	*/

	public function pack ($file, $data = array ()) // Добавление файла или создание папки
	{
		if ($data === array () && file_exists ($file))
			$data = file_get_contents ($file);

		self::recursivePack ($this->packet, str_replace ('\\', '/', $file), $data);
	}

	/*
		array @packet    - сервисное значение, текущая часть пакета
		string @filename - путь (часть пути) или название файла для сохранения
		mixed @data      - одержимое файла
	*/

	protected function recursivePack (&$packet, $filename, $data) // Сервисная функция добавления файла или создание папки
	{
		if (strpos ($filename, '/') !== false)
		{
			$filename = explode ('/', $filename);

			foreach ($filename as $id => $path)
				if (strlen ($path = trim ($path)))
				{
					if (strlen ($filename[$id+1]))
						self::recursivePack ($packet['/'. $path], implode ('/', array_slice ($filename, $id + 1)), $data);

					else
					{
						if ($data === array ())
							$packet['/'. trim ($path)] = array ();

						else $packet[trim ($path)] = self::packText ($data);
					}

					break;
				}
		}

		else
		{
			if ($data === array ())
				$packet['/'. trim ($filename)] = array ();

			else $packet[trim ($filename)] = self::packText ($data);
		}
	}

	/*
		string [@unpackDir = ''] - директория для распаковки (ничего - директория файла-исполнителя)
	*/

	public function unpack ($unpackDir = '') // Распаковка пакета
	{
		$unpackDir = trim (str_replace ('\\', '/', $unpackDir));

		if (strlen ($unpackDir))
			@mkdir ($unpackDir, 0777, true);

		self::recursiveUnpack ($this->packet, $unpackDir);
	}

	/*
		array @packet     - сервисное значение, текущая часть пакета
		string @unpackDir - путь (часть пути) или для распаковки
	*/

	protected function recursiveUnpack ($packet, $unpackDir) // Сервисная функция распаковки пакета
	{
		foreach ($packet as $file => $data)
			if (is_array ($data))
			{
				@mkdir ($unpackDir .'/'. $file, 0777, true);

				self::recursiveUnpack ($data, $unpackDir .'/'. $file);
			}

			elseif (strlen ($data))
				@file_put_contents ($unpackDir .'/'. $file, self::unpackText ($data));
	}

	/*
		string @dir - директория для удаления
	*/

	public function remove ($path) // Удаление файла
	{
		$path = trim (str_replace ('\\', '/', $path));

		self::recursiveRemove ($this->packet, $path);
	}

	/*
		array @packet - сервисное значение, текущая часть пакета
		string @dir   - путь (часть пути) к файлу
	*/

	protected function recursiveRemove (&$packet, $dir) // Сервисная функция удаления файла
	{
		if (strpos ($dir, '/') !== false)
		{
			$dir = explode ('/', $dir);

			foreach ($dir as $id => $path)
				if (strlen ($path = trim ($path)))
				{
					if (strlen ($dir[$id+1]))
						self::recursiveLookup ($packet['/'. $path], implode ('/', array_slice ($dir, $id + 1)));

					else unset ($packet[$path], $packet['/'. $path]);
				}
		}

		else unset ($packet[$dir], $packet['/'. $dir]);
	}

	/*
		string @path - путь для вывода
	*/

	public function getEntitiesList ($path) // Вывод содержимого определённой директории
	{
		$path = trim (str_replace ('\\', '/', $path));

		return self::recursiveLookup ($this->packet, $path);
	}

	/*
		array @packet - сервисное значение, текущая часть пакета
		string @dir   - путь (часть пути) к папке
	*/

	protected function recursiveLookup (&$packet, $dir) // Сервисная функция вывода содержимого директории
	{
		if (strpos ($dir, '/') !== false)
		{
			$dir = explode ('/', $dir);

			foreach ($dir as $id => $path)
				if (strlen ($path = trim ($path)))
				{
					if (strlen ($dir[$id+1]))
						return self::recursiveLookup ($packet['/'. $path], implode ('/', array_slice ($dir, $id + 1)));

					else return $packet['/'. $path];
				}
		}

		else return $packet['/'. $dir];
	}

	public function save ($file, $ext = 'eap') // Сохранение пакета
	{
		if (end (explode ('.', $file)) !== $ext)
			$file .= ".$ext";

		@file_put_contents ($file, self::packText ($this->packet, false));
	}

	/*
		mixed @data        - информация для сжатия
		bool [@raw = true] - сжимать ли информация (true - нет) 
	*/

	public function packText ($data, $raw = true) // Сервисная функция сжатия информации
	{
		$data = serialize ($data);

		return (bool)($raw) ? $data : gzdeflate ($data, 9);
	}

	/*
		mixed @data        - информация для расжатия
		bool [@raw = true] - расжимать ли информация (true - нет) 
	*/

	public function unpackText ($data, $raw = true) // Сервисная функция расжатия информации
	{
		return ($raw) ? unserialize ($data) : unserialize (gzinflate ($data));
	}
}

?>
